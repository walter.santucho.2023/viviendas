public class Vivienda {
    private Figura figura;
    public Vivienda(int forma, double a, double b) {
        figura = new Figura(forma);
        // Un cuadrado es un rectangulo con lados iguales
        if (figura.getForma() == 1)
            figura = new Rectangulo(a, b);
        else
            figura = new Triangulo(a, b);
    }
    public double calcularArea() {
        return figura.calcularArea();
    }
}
