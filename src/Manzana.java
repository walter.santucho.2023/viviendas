import java.util.ArrayList;
import java.util.List;
public class Manzana {
    private List<Vivienda> viviendas = new ArrayList<>();
    private Figura figura;
    public List<Vivienda> getViviendas() {
        return viviendas;
    }
    public void setViviendas(List<Vivienda> viviendas) {
        this.viviendas = viviendas;
    }
    public Manzana(int forma,double a, double b)
    {
        figura = new Figura(forma);
        // Un cuadrado es un rectangulo con lados iguales
        if (figura.getForma() == 1)
            figura = new Rectangulo(a, b);
        else
            figura = new Triangulo(a, b);
    }
    public double calcularArea() {
        return figura.calcularArea();
    }
    public double calcularSuperficieOcupada()
    {
        double superficieOcupada = 0;
        for (Vivienda vivienda : viviendas) {
            superficieOcupada += vivienda.calcularArea();
        }
        return superficieOcupada;
    }
    public double calcularSuperficieDisponible()
    {
        return this.calcularArea() - calcularSuperficieOcupada();
    }
    public Vivienda obtenerViviendaMayorSuperficie()
    {
        Vivienda viviendaMayor = null;
        double maxSuperficie = 0;
        for (Vivienda vivienda : viviendas)
        {
            if (vivienda.calcularArea() > maxSuperficie) {
                maxSuperficie = vivienda.calcularArea();
                viviendaMayor = vivienda;
            }
        }
        return viviendaMayor;
    }
    public boolean asignarVivienda(Vivienda unaVivienda)
    {
        if (calcularSuperficieDisponible() >= unaVivienda.calcularArea()) {
            viviendas.add(unaVivienda);
            return true;
        }
        return false;
    }
}
