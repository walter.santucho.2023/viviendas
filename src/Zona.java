import java.util.ArrayList;
import java.util.List;

public class Zona {
    private List<Manzana> manzanas = new ArrayList<>();
    public void agregarManzana(Manzana manzana) {
        manzanas.add(manzana);
    }
    public int contarFamiliasNumerosas(double tamanoMinimoVivienda) {
        int familiasNumerosas = 0;
        for (Manzana manzana : manzanas) {
            for (Vivienda vivienda : manzana.getViviendas()) {
                if (vivienda.calcularArea() >= tamanoMinimoVivienda) {
                    familiasNumerosas++;
                }
            }
        }
        return familiasNumerosas;
    }
}
